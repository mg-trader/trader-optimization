package eu.verante.trader.optimization.domain;

import lombok.Value;

import java.util.List;

@Value
public class TradingChromosome {

    List<TradingGene> tradingGenes;

}
