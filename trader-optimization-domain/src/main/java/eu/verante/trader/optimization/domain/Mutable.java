package eu.verante.trader.optimization.domain;

import java.util.Random;

public interface Mutable<T extends Mutable<T>> {
    int getMutationPoints();

    T mutate(int pointToMutate, Random random);
}
