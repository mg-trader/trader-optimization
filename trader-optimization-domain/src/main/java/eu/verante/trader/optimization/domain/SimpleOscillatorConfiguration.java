package eu.verante.trader.optimization.domain;

import lombok.Value;

import java.util.Map;
import java.util.Random;

@Value
public class SimpleOscillatorConfiguration
        extends AbstractIndicatorGeneConfiguration<SimpleOscillatorConfiguration>{

    private int periods;
    private double decisionThreshold;


    @Override
    public Map<String, Object> asDecisionMakerConfiguration() {
        return Map.of("decisionThreshold", decisionThreshold);
    }

    @Override
    public int getMutationPoints() {
        return 2;
    }

    @Override
    public SimpleOscillatorConfiguration mutate(int pointToMutate, Random random) {
        int newPeriods = pointToMutate == 0 ? mutatePeriods(random): periods;
        double newThreshold = pointToMutate == 1 ? mutateThreshold(random) : decisionThreshold;
        return new SimpleOscillatorConfiguration(newPeriods, newThreshold);
    }

    private double mutateThreshold(Random random) {
        double change = random.nextDouble(decisionThreshold/2.0);
        change = random.nextBoolean() ? change : -change;
        return Math.max(0.1, this.decisionThreshold + change);
    }

    private int mutatePeriods(Random random) {
        int change = random.nextInt(1,periods/2);
        change = random.nextBoolean() ? change : -change;
        return Math.max(1, this.periods + change);
    }
}
