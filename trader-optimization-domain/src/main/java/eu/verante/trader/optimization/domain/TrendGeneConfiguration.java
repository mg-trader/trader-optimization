package eu.verante.trader.optimization.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;

import java.util.Random;

@Value
public class TrendGeneConfiguration extends AbstractIndicatorGeneConfiguration<TrendGeneConfiguration> {
    IndicatorGeneElement<?> indicator;
    double threshold;

    @JsonCreator
    public TrendGeneConfiguration(IndicatorGeneElement<?> indicator, double threshold) {
        this.indicator = indicator;
        this.threshold = threshold;
    }

    @Override
    public int getMutationPoints() {
        return 1 + indicator.getMutationPoints();
    }

    @Override
    public TrendGeneConfiguration mutate(int pointToMutate, Random random) {
        if (pointToMutate == 0) {
            double multiplier = random.nextDouble(0.1, 2.0);
            return new TrendGeneConfiguration(indicator, threshold*multiplier);
        }
        return new TrendGeneConfiguration(indicator.mutate(pointToMutate-1, random), threshold);
    }
}
