package eu.verante.trader.optimization.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;

import java.util.Random;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@JsonTypeName("trend")
public class TrendIndicatorGeneElement extends IndicatorGeneElement<TrendGeneConfiguration>{
    public TrendIndicatorGeneElement(TrendGeneConfiguration configuration) {
        super("TREND", configuration);
    }

    @JsonCreator
    private static TrendIndicatorGeneElement create(String indicatorType, TrendGeneConfiguration configuration) {
        return new TrendIndicatorGeneElement(configuration);
    }

    @Override
    public TrendIndicatorGeneElement mutate(int pointToMutate, Random random) {
        return new TrendIndicatorGeneElement(getConfiguration().mutate(pointToMutate, random));
    }
}
