package eu.verante.trader.optimization.domain.api;

import lombok.Data;

import java.util.List;

@Data
public class BatchExecutionRequest extends AbstractExecutionRequest {

    private List<String> symbols;
}
