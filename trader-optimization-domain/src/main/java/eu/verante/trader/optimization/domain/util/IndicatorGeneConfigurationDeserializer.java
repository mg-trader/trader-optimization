package eu.verante.trader.optimization.domain.util;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;
import eu.verante.trader.optimization.domain.IndicatorGeneConfiguration;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

@Component
public class IndicatorGeneConfigurationDeserializer extends StdDeserializer<IndicatorGeneConfiguration> {

    private final Map<String, Class<? extends IndicatorGeneConfiguration>> configTypes = Map.of(
//            SIMPLE_CONFIGURATION_TYPE, SimpleGeneConfiguration.class,
//            COMPARE_CONFIGURATION_TYPE, CompareGeneConfiguration.class,
//            TREND_CONFIGURATION_TYPE, TrendGeneConfiguration.class,
//            MOVING_AVERAGE_CONFIGURATION_TYPE, MovingAverageConfiguration.class
    );
    public IndicatorGeneConfigurationDeserializer() {
        super(IndicatorGeneConfiguration.class);
    }

    @Override
    public IndicatorGeneConfiguration deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JacksonException {
        ObjectNode node = p.getCodec().readTree(p);
        String configurationType = node.get("configurationType").asText();
        Class<? extends IndicatorGeneConfiguration> type = configTypes.get(configurationType);
        node.remove("configurationType");
        return ctxt.readTreeAsValue(node, type);
    }
}
