package eu.verante.trader.optimization.domain;

import java.util.Map;

public interface IndicatorGeneConfiguration<C extends IndicatorGeneConfiguration<C>> extends Mutable<C> {

    default Map<String, Object> asDecisionMakerConfiguration() {
        return null;
    }
}
