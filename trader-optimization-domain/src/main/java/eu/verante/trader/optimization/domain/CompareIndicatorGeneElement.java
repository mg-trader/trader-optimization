package eu.verante.trader.optimization.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;

import java.util.Random;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@JsonTypeName("compare")
public class CompareIndicatorGeneElement extends IndicatorGeneElement<CompareGeneConfiguration> {
    public CompareIndicatorGeneElement(CompareGeneConfiguration configuration) {
        super("COMPARE_INDICATORS", configuration);
    }

    @JsonCreator
    private static CompareIndicatorGeneElement create(String indicatorType, CompareGeneConfiguration configuration) {
        return new CompareIndicatorGeneElement(configuration);
    }

    @Override
    public CompareIndicatorGeneElement mutate(int pointToMutate, Random random) {
        return new CompareIndicatorGeneElement(getConfiguration().mutate(pointToMutate, random));
    }
}
