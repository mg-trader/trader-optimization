package eu.verante.trader.optimization.domain.api;

import eu.verante.trader.optimization.domain.Execution;
import eu.verante.trader.util.asyncprocess.AsyncProcess;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.UUID;

public interface ExecutionResource {

    @GetMapping("/{id}")
    Execution getExecution(@PathVariable("id") UUID id);

    @GetMapping("/")
    List<Execution> getExecutions();

    @GetMapping("/run")
    AsyncProcess runExecution(@RequestBody ManualExecutionRequest request);

    @GetMapping("/runAll")
    AsyncProcess runAllExecutions(@RequestBody BatchExecutionRequest request);

}
