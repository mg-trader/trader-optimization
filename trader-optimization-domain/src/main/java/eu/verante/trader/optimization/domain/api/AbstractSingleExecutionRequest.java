package eu.verante.trader.optimization.domain.api;

import eu.verante.trader.stock.candle.Period;
import lombok.Data;

@Data
public abstract class AbstractSingleExecutionRequest extends AbstractExecutionRequest {

    private String symbol;

}
