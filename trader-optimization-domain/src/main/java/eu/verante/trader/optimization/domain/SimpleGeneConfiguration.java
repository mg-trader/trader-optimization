package eu.verante.trader.optimization.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import eu.verante.trader.indicator.domain.value.SimpleValueType;
import lombok.Value;

import java.util.Random;

@Value
public class SimpleGeneConfiguration extends AbstractIndicatorGeneConfiguration<SimpleGeneConfiguration> {
    SimpleValueType value;

    @JsonCreator
    public SimpleGeneConfiguration(SimpleValueType value) {
        this.value = value;
    }

    @Override
    public int getMutationPoints() {
        return 1;
    }

    @Override
    public SimpleGeneConfiguration mutate(int pointToMutate, Random random) {
        int ordinal = random.nextInt(SimpleValueType.values().length);
        SimpleValueType newValue = SimpleValueType.values()[ordinal];
        return new SimpleGeneConfiguration(newValue);
    }
}
