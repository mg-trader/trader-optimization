package eu.verante.trader.optimization.domain;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Random;

@Getter
@FieldDefaults(makeFinal=true, level= AccessLevel.PRIVATE)
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class IndicatorTradingGene extends AbstractTradingGene<IndicatorTradingGene> {

    private IndicatorGeneElement<?> indicator;

    public IndicatorTradingGene(Integer weight, IndicatorGeneElement<?> indicator) {
        super(weight);
        this.indicator = indicator;
    }

    @Override
    public IndicatorTradingGene withWeight(Integer newWeight) {
        return new IndicatorTradingGene(newWeight, indicator);
    }

    @Override
    public int getMutationPoints() {
        return 1 + indicator.getMutationPoints();
    }

    @Override
    public IndicatorTradingGene mutate(int pointToMutate, Random random) {
        if (pointToMutate == 0) {
            return mutateWeight(random);
        }
        return new IndicatorTradingGene(getWeight(), indicator.mutate(pointToMutate-1, random));
    }

    private IndicatorTradingGene mutateWeight(Random random) {
        int diff = random.nextInt(1, 5);
        int weightChange = random.nextBoolean() ? diff : -diff;
        int newWeight = getWeight() + weightChange;

        IndicatorTradingGene gene = withWeight(newWeight);
        return gene;
    }
}
