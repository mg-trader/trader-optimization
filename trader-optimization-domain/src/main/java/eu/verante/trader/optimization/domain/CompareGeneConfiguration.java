package eu.verante.trader.optimization.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;

import java.util.Random;

@Value
public class CompareGeneConfiguration extends AbstractIndicatorGeneConfiguration<CompareGeneConfiguration> {
    IndicatorGeneElement<?> value;
    IndicatorGeneElement<?> compareTo;

    private CompareGeneConfiguration(IndicatorGeneElement<?> value, IndicatorGeneElement<?> compareTo) {
        this.value = value;
        this.compareTo = compareTo;
    }

    @JsonCreator
    public static CompareGeneConfiguration compare(IndicatorGeneElement value, IndicatorGeneElement compareTo) {
        return new CompareGeneConfiguration(value, compareTo);
    }

    @Override
    public int getMutationPoints() {
        return value.getMutationPoints() + compareTo.getMutationPoints();
    }

    @Override
    public CompareGeneConfiguration mutate(int pointToMutate, Random random) {
        if (pointToMutate < value.getMutationPoints()) {
            return new CompareGeneConfiguration(value.mutate(pointToMutate, random), compareTo);
        }
        return new CompareGeneConfiguration(value, compareTo.mutate(pointToMutate-value.getMutationPoints(), random));
    }
}
