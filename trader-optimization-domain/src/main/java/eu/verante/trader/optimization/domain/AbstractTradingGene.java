package eu.verante.trader.optimization.domain;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(makeFinal=true, level=AccessLevel.PRIVATE)
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public abstract class AbstractTradingGene<TG extends AbstractTradingGene<TG>> implements TradingGene<TG> {

    private Integer weight;

}
