package eu.verante.trader.optimization.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Random;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@JsonTypeName("moving-average")
public class MovingAverageGeneElement extends IndicatorGeneElement<MovingAverageConfiguration>{
    @JsonCreator
    public MovingAverageGeneElement(String indicatorType, MovingAverageConfiguration configuration) {
        super(indicatorType, configuration);
    }

    @Override
    public MovingAverageGeneElement mutate(int pointToMutate, Random random) {
        return new MovingAverageGeneElement(getIndicatorType(), getConfiguration().mutate(pointToMutate, random));
    }
}
