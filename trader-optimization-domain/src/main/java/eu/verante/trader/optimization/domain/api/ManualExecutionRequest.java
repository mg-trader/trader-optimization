package eu.verante.trader.optimization.domain.api;

import lombok.Data;

import java.util.UUID;

@Data
public class ManualExecutionRequest extends AbstractSingleExecutionRequest {
    private UUID baseOptimizationId;
}
