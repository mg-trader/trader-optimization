package eu.verante.trader.optimization.domain;

import eu.verante.trader.optimization.domain.api.OptimizationRequest;
import lombok.Data;

import java.util.UUID;

@Data
public class Optimization extends AbstractExecution<OptimizationRequest>{

    private UUID chromosomeExecutionId;

}
