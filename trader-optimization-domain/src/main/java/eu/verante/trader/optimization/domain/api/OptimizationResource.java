package eu.verante.trader.optimization.domain.api;

import eu.verante.trader.optimization.domain.Optimization;
import eu.verante.trader.util.asyncprocess.AsyncProcess;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.UUID;

public interface OptimizationResource {
    @GetMapping("/run")
    AsyncProcess runOptimization(@RequestBody OptimizationRequest request);

    @GetMapping("/{processId}/status")
    AsyncProcess getProcessStatus(@PathVariable("processId") UUID processId);

    @GetMapping("/{id}")
    Optimization getOptimizationById(@PathVariable("id") UUID processId);

    @GetMapping("/")
    List<Optimization> getOptimizations();

}
