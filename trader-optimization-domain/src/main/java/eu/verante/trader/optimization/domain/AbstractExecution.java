package eu.verante.trader.optimization.domain;

import eu.verante.trader.decision.domain.DecisionFitness;
import eu.verante.trader.optimization.domain.api.AbstractSingleExecutionRequest;
import lombok.Data;

import java.util.UUID;

@Data
public class AbstractExecution<R extends AbstractSingleExecutionRequest> {

    private UUID id;

    private long started;
    private long ended;
    private R params;

    private TradingChromosome chromosome;

    private DecisionFitness fitness;
}
