package eu.verante.trader.optimization.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.verante.trader.indicator.domain.value.SimpleValueType;
import lombok.Value;

import java.util.Random;

@Value
public class MovingAverageConfiguration extends AbstractIndicatorGeneConfiguration<MovingAverageConfiguration> {
    int periods;
    int shift;
    SimpleValueType base;

    @JsonCreator
    public MovingAverageConfiguration(@JsonProperty("periods") int periods,
                                      @JsonProperty(value = "shift", defaultValue = "0") int shift,
                                      @JsonProperty(value = "base", defaultValue = "CLOSE") SimpleValueType base) {
        this.periods = periods;
        this.shift = shift;
        this.base = base;
    }

    @Override
    public int getMutationPoints() {
        return 3;
    }

    @Override
    public MovingAverageConfiguration mutate(int pointToMutate, Random random) {
        int newPeriods = pointToMutate == 0 ? mutatePeriods(random): periods;
        int newShift = pointToMutate == 1 ? mutateShift(random) : shift;
        SimpleValueType newBase = pointToMutate ==2 ? mutateBase(random) : base;
        return new MovingAverageConfiguration(newPeriods, newShift, newBase);
    }

    private SimpleValueType mutateBase(Random random) {
        int ordinal = random.nextInt(SimpleValueType.values().length);
        return SimpleValueType.values()[ordinal];
    }

    private int mutateShift(Random random) {
        int change = random.nextInt(5);
        int direction = random.nextBoolean() ? 1 : -1;
        return shift + (change*direction);
    }

    private int mutatePeriods(Random random) {
        int change = random.nextInt(1,periods/2);
        change = random.nextBoolean() ? change : -change;
        return Math.max(1, this.periods + change);
    }
}
