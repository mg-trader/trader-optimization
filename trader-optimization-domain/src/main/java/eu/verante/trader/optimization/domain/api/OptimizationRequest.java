package eu.verante.trader.optimization.domain.api;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class OptimizationRequest extends AbstractSingleExecutionRequest {

    private int initialPopulationSize = 10000;
    private int generations = 10;

    private int mutationPromile = 10;

}
