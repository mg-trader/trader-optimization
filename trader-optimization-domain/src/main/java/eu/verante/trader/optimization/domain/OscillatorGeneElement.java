package eu.verante.trader.optimization.domain;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Random;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@JsonTypeName("oscillator")
public class OscillatorGeneElement
        extends IndicatorGeneElement<SimpleOscillatorConfiguration>{
    public OscillatorGeneElement(String indicatorType, SimpleOscillatorConfiguration configuration) {
        super(indicatorType, configuration);
    }

    @Override
    public OscillatorGeneElement mutate(int pointToMutate, Random random) {
        return new OscillatorGeneElement(getIndicatorType(), getConfiguration().mutate(pointToMutate, random));
    }
}
