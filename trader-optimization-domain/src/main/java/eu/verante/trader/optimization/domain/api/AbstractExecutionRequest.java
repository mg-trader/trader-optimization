package eu.verante.trader.optimization.domain.api;

import eu.verante.trader.stock.candle.Period;
import lombok.Data;

@Data
public abstract class AbstractExecutionRequest {

    private long from;
    private long to = System.currentTimeMillis();
    private Period period;

    private boolean cleanupBefore;
}
