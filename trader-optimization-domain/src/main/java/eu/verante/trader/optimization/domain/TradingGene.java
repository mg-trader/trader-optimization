package eu.verante.trader.optimization.domain;

public interface TradingGene<TG extends TradingGene<TG>> extends Mutable<TG> {

    Integer getWeight();

    TradingGene withWeight(Integer newWeight);
}
