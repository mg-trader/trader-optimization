package eu.verante.trader.optimization.domain;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import eu.verante.trader.indicator.domain.value.SimpleValueType;
import lombok.*;
import lombok.experimental.FieldDefaults;


@FieldDefaults(makeFinal=true, level=AccessLevel.PRIVATE)
@ToString
@EqualsAndHashCode
@Getter
@AllArgsConstructor
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(SimpleIndicatorGeneElement.class),
        @JsonSubTypes.Type(CompareIndicatorGeneElement.class),
        @JsonSubTypes.Type(MovingAverageGeneElement.class),
        @JsonSubTypes.Type(OscillatorGeneElement.class),
        @JsonSubTypes.Type(TrendIndicatorGeneElement.class)
})
public abstract class IndicatorGeneElement<C extends IndicatorGeneConfiguration<C>>
        implements Mutable<IndicatorGeneElement<C>> {
    String indicatorType;
    C configuration;

    public static IndicatorGeneElement<MovingAverageConfiguration> movingAverage(String type, int periods, int shift, SimpleValueType base) {
        return new MovingAverageGeneElement(type, new MovingAverageConfiguration(periods, shift, base));
    }

    public static IndicatorGeneElement<SimpleGeneConfiguration> simpleClose() {
        return new SimpleIndicatorGeneElement(new SimpleGeneConfiguration(SimpleValueType.CLOSE));
    }

    public static IndicatorGeneElement<CompareGeneConfiguration> compare(IndicatorGeneElement<?> value, IndicatorGeneElement<?> compareTo) {
        return new CompareIndicatorGeneElement(
                CompareGeneConfiguration.compare(value, compareTo));
    }

    @Override
    public int getMutationPoints() {
        return configuration.getMutationPoints();
    }
}
