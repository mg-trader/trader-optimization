package eu.verante.trader.optimization.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;

import java.util.Random;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@JsonTypeName("simple")
public class SimpleIndicatorGeneElement extends IndicatorGeneElement<SimpleGeneConfiguration>{
    public SimpleIndicatorGeneElement(SimpleGeneConfiguration configuration) {
        super("SIMPLE", configuration);
    }

    @JsonCreator
    private static SimpleIndicatorGeneElement create(String indicatorType, SimpleGeneConfiguration configuration) {
        return new SimpleIndicatorGeneElement(configuration);
    }

    @Override
    public SimpleIndicatorGeneElement mutate(int pointToMutate, Random random) {
        return new SimpleIndicatorGeneElement(getConfiguration().mutate(pointToMutate, random));
    }
}
