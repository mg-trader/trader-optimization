package eu.verante.trader.optimization.domain;

import eu.verante.trader.optimization.domain.api.AbstractExecutionRequest;
import eu.verante.trader.optimization.domain.api.ManualExecutionRequest;
import lombok.Data;

import java.util.UUID;

@Data
public class Execution extends AbstractExecution<ManualExecutionRequest> {

}
