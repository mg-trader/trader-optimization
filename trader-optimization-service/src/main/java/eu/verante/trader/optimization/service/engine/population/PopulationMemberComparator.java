package eu.verante.trader.optimization.service.engine.population;

import eu.verante.trader.decision.domain.DecisionFitness;

import java.util.Comparator;

public class PopulationMemberComparator implements Comparator<PopulationMember> {
    @Override
    public int compare(PopulationMember o1, PopulationMember o2) {
        return -Double.compare(getTotalProfitPercent(o1), getTotalProfitPercent(o2));
    }

    private static double getTotalProfitPercent(PopulationMember o1) {
        return o1.getFitness().map(p-> p.getTotalStats().getProfitPercent()).orElse(0.0d);
    }
}
