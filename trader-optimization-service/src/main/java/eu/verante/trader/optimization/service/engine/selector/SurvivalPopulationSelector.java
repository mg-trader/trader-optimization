package eu.verante.trader.optimization.service.engine.selector;

import eu.verante.trader.optimization.service.engine.population.Population;

public interface SurvivalPopulationSelector {

    Population select(Population original);

}
