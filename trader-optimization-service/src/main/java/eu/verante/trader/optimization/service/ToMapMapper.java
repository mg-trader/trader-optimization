package eu.verante.trader.optimization.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.verante.trader.optimization.domain.IndicatorGeneElement;
import eu.verante.trader.optimization.domain.SimpleGeneConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public abstract class ToMapMapper {
    @Autowired
    private ObjectMapper objectMapper;

//    private Map<String, Class<? extends IndicatorGeneElement>> elementsByConfiguration = Map.of(
//            SimpleGeneConfiguration.SIMPLE_CONFIGURATION_TYPE, Si
//    );

    public Map<String, Object> toMap(IndicatorGeneElement geneElement) {
        return objectMapper.convertValue(geneElement, Map.class);
    }

    public IndicatorGeneElement toIndicatorGene(Map<String, Object> map) {
//        Map<String, Object> config = (Map<String, Object>) map.get("configuration");
//        config.get("configurationType");
        try {
            return objectMapper.readValue(objectMapper.writeValueAsString(map), IndicatorGeneElement.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
