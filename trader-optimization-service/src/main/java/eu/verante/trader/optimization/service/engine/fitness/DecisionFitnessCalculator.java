package eu.verante.trader.optimization.service.engine.fitness;

import eu.verante.trader.decision.domain.DecisionFitness;
import eu.verante.trader.optimization.domain.TradingChromosome;

public interface DecisionFitnessCalculator {

    DecisionFitness get(TradingChromosome member);
}
