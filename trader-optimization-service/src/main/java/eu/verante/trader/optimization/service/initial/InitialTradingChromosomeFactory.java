package eu.verante.trader.optimization.service.initial;

import eu.verante.trader.optimization.domain.TradingChromosome;
import eu.verante.trader.optimization.domain.TradingGene;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Component
public class InitialTradingChromosomeFactory {

    private final Log logger = LogFactory.getLog(InitialTradingChromosomeFactory.class);

    @Autowired
    private List<InitialTradingGeneFactory> initialFactories;

    private final Random random = new Random(System.currentTimeMillis());

    public List<TradingChromosome> generateInitialPopulation(int initialPopulationSize) {
        List<TradingChromosome> result = new ArrayList<>();

        List<List<TradingGene>> allGenes = initialFactories.stream()
                .map(factory -> factory.generateInitialGenes())
                .collect(Collectors.toList());

        while(result.size() < initialPopulationSize) {
            result.add(generateChromosome(allGenes));
        }
        logger.info("Generated initial population of: " +result.size());
        return result;
    }

    private TradingChromosome generateChromosome(List<List<TradingGene>> allGenes) {
        List<TradingGene> selectedGenes = new ArrayList<>();
        for (List<TradingGene> allGene : allGenes) {
            selectedGenes.add(allGene.get(random.nextInt(allGene.size())));
        }
        return new TradingChromosome(selectedGenes);
    }
}
