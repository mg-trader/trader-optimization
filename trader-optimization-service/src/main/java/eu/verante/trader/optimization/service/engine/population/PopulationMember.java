package eu.verante.trader.optimization.service.engine.population;

import eu.verante.trader.decision.domain.DecisionFitness;
import eu.verante.trader.optimization.domain.TradingChromosome;

import java.util.Optional;

public class PopulationMember {
    private final TradingChromosome chromosome;

    private Optional<DecisionFitness> fitness = Optional.empty();


    public PopulationMember(TradingChromosome chromosome) {
        this.chromosome = chromosome;
    }

    public TradingChromosome getChromosome() {
        return chromosome;
    }

    public Optional<DecisionFitness> getFitness() {
        return fitness;
    }

    public void setFitness(DecisionFitness fitness) {
        this.fitness = Optional.of(fitness);
    }
}
