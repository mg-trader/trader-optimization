package eu.verante.trader.optimization.service.engine.fitness;

import eu.verante.trader.optimization.domain.TradingGene;

public abstract class AbstractTradingGeneExecutor<TG extends TradingGene>
        implements TradingGeneExecutor<TG> {

    private final Class<TG> handledClass;

    protected AbstractTradingGeneExecutor(Class<TG> handledClass) {
        this.handledClass = handledClass;
    }

    @Override
    public Class<TG> getHandledClass() {
        return handledClass;
    }
}
