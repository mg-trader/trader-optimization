package eu.verante.trader.optimization.service.engine.fitness;

import eu.verante.trader.decision.domain.CalculateSyntheticFitnessRequest;
import eu.verante.trader.decision.domain.DecisionFitness;
import eu.verante.trader.decision.domain.DecisionPredicate;
import eu.verante.trader.optimization.domain.TradingChromosome;
import eu.verante.trader.optimization.domain.TradingGene;
import eu.verante.trader.optimization.domain.api.AbstractSingleExecutionRequest;
import eu.verante.trader.optimization.service.client.FitnessClient;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;
import java.util.SortedMap;
import java.util.stream.Collectors;

@Component
@Scope("prototype")
public class DefaultDecisionFitnessCalculator implements DecisionFitnessCalculator{

    private final Log logger = LogFactory.getLog(DefaultDecisionFitnessCalculator.class);

    private final AbstractSingleExecutionRequest request;

    @Autowired
    private FitnessClient fitnessClient;

    private Map<Class<?>, TradingGeneExecutor> geneExecutors;

    public DefaultDecisionFitnessCalculator(AbstractSingleExecutionRequest request) {
        this.request = request;
    }

    @Autowired
    public void setGeneExecutors(Collection<TradingGeneExecutor> geneExecutors) {
        this.geneExecutors = geneExecutors.stream()
                .collect(Collectors.toMap(
                        ge -> ge.getHandledClass(),
                        ge -> ge
                ));
    }
    @Override
    public DecisionFitness get(TradingChromosome member) {
        ChromosomeDecisionVector decisionVector = transformToDecisionVector(member, request);
        TradeStateTransformer stateTransformer = new CombinedStateTransformer();

        SortedMap<Long, DecisionPredicate> tradeStates = decisionVector.toTradeStates(stateTransformer);

        CalculateSyntheticFitnessRequest fitnessRequest = new CalculateSyntheticFitnessRequest();
        fitnessRequest.setDecisions(tradeStates);
        fitnessRequest.setSymbol(request.getSymbol());
        fitnessRequest.setPeriod(request.getPeriod());
        fitnessRequest.setFrom(request.getFrom());
        fitnessRequest.setTo(request.getTo());

        DecisionFitness fitness = fitnessClient.calculateFitness(fitnessRequest);
        logger.debug("Calculated fitness: " + fitness.getTotalStats().getProfitPercent());

        return fitness;
    }

    private ChromosomeDecisionVector transformToDecisionVector(TradingChromosome member, AbstractSingleExecutionRequest request) {
        ChromosomeDecisionVector decisionVector = new ChromosomeDecisionVector();

        for (TradingGene tradingGene : member.getTradingGenes()) {
            SortedMap<Long, DecisionPredicate> geneDecision = getGeneDecision(tradingGene, request);
            Integer weight = tradingGene.getWeight();

            for (Long time : geneDecision.keySet()) {
                ChromosomeDecision chromosomeDecision = decisionVector.getChromosomeDecision(time);
                chromosomeDecision.putDecision(weight, geneDecision.get(time));
            }
        }

        return decisionVector;
    }

    private SortedMap<Long, DecisionPredicate> getGeneDecision(TradingGene gene, AbstractSingleExecutionRequest request) {
        return geneExecutors.get(gene.getClass()).execute(gene, request);
    }
}
