package eu.verante.trader.optimization.service.initial;

import eu.verante.trader.optimization.domain.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static eu.verante.trader.optimization.domain.IndicatorGeneElement.simpleClose;

@Component
public class TrendOnCloseFactory implements InitialTradingGeneFactory {
    @Override
    public List<TradingGene> generateInitialGenes() {
        List<TradingGene> result = new ArrayList<>();
        for (double threshold = 0.1; threshold < 10.0 ; threshold=threshold*2.0) {
            result.add(new IndicatorTradingGene(10,
                    new TrendIndicatorGeneElement(
                            new TrendGeneConfiguration(simpleClose(), threshold)
                    )));
        }
        return result;
    }
}
