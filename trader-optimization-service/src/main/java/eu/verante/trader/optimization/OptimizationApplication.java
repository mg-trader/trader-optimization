package eu.verante.trader.optimization;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(scanBasePackages = "eu.verante.trader")
@EnableFeignClients
@EnableAsync
@EnableCaching
@EnableScheduling
@EnableMongoRepositories
public class OptimizationApplication {

    public static void main(String[] args) {
        SpringApplication.run(OptimizationApplication.class, args);
    }
}
