package eu.verante.trader.optimization.service.engine.selector;

import eu.verante.trader.optimization.service.engine.population.Population;
import eu.verante.trader.optimization.service.engine.population.PopulationMember;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SimpleSurvivalPopulationSelector implements SurvivalPopulationSelector {

    private final int selectBest;

    private final Random random = new Random(System.currentTimeMillis());

    public SimpleSurvivalPopulationSelector(int bestToSelectPercent) {
        this.selectBest = bestToSelectPercent;
    }

    @Override
    public Population select(Population original) {
        int neededSize = original.size()/2;
        List<PopulationMember> selected = new ArrayList<>();
        selected.addAll(original.findBest(selectBest));
        while (selected.size() < neededSize) {
            selected.add(original.get(random.nextInt(original.size())));
        }

        return Population.fromMembers(selected);
    }
}
