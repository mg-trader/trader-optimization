package eu.verante.trader.optimization.service.engine.fitness;

import eu.verante.trader.decision.domain.DecisionPredicate;
import eu.verante.trader.decision.domain.DecisionType;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;

public class CombinedStateTransformer extends AbstractTradeStateTransformer {

    private List<Pair<Integer, DecisionPredicate>> predicates = null;
    private DecisionType previousBuySellDecision = DecisionType.HOLD;

    @Override
    public DecisionPredicate transformNext(List<Pair<Integer, DecisionPredicate>> geneDecisions) {
        if (predicates == null) {
            predicates = new ArrayList<>(geneDecisions);
        } else {
            for (int geneIndex = 0; geneIndex < predicates.size(); geneIndex++) {
                Pair<Integer, DecisionPredicate> geneDecision = geneDecisions.get(geneIndex);
                if (geneDecision.getValue().getType()!= DecisionType.HOLD) {
                    predicates.set(geneIndex, geneDecision);
                }
            }
        }

        DecisionPredicate decision = getChromosomeDecision(predicates);
        if (decision.getType() != DecisionType.HOLD) {
            previousBuySellDecision = decision.getType();
        }
        DecisionType decisionType = previousBuySellDecision;
        return new DecisionPredicate(decisionType, decision.getContext());
    }
}
