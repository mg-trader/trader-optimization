package eu.verante.trader.optimization.service;

import eu.verante.trader.decision.domain.DecisionFitness;
import eu.verante.trader.optimization.domain.api.OptimizationRequest;
import eu.verante.trader.optimization.model.OptimizationDocument;
import eu.verante.trader.optimization.repository.OptimizationRepository;
import eu.verante.trader.optimization.service.engine.GeneticEngine;
import eu.verante.trader.optimization.service.engine.population.PopulationMember;
import eu.verante.trader.optimization.service.initial.InitialTradingChromosomeFactory;
import eu.verante.trader.util.asyncprocess.AsyncProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OptimizationRequestHandler extends AbstractExecutionRequestHandler<OptimizationRequest> {

    @Autowired
    private InitialTradingChromosomeFactory chromosomeFactory;

    @Autowired
    private GeneticEngine geneticEngine;



    @Autowired
    private OptimizationMapper optimizationMapper;

    @Autowired
    private OptimizationRepository optimizationRepository;

    @Override
    protected void doProcess(OptimizationRequest request, AsyncProcess process) {
        OptimizationDocument newOptimization = createNewOptimization(request, process);

        cleanUpIfNecessary(request);

        PopulationMember best = geneticEngine.findBest(
                chromosomeFactory.generateInitialPopulation(request.getInitialPopulationSize()),
                request);

        System.out.println("WINNER CHROMOSOME: " + best.getChromosome());
        System.out.println("WINNER PERFORMANCE: " + best.getFitness().get().getTotalStats().getProfitPercent());
        System.out.println("WINNER TRADES");
        best.getFitness().get().getPositions().forEach(p -> System.out.println(p));
        System.out.println("Current trade:");
        System.out.println(best.getFitness().get().getCurrentPosition());
        System.out.println("Total stats: " + best.getFitness().get().getTotalStats());
        System.out.println("Buy stats: " + best.getFitness().get().getBuyStats());
        System.out.println("Sell stats: " + best.getFitness().get().getSellStats());

        storeOptimizationResults(newOptimization, best);

    }

    private void storeOptimizationResults(OptimizationDocument document, PopulationMember best) {
        DecisionFitness fitness = best.getFitness().get();

        document.setEnded(System.currentTimeMillis());
        document.setChromosome(optimizationMapper.toChromosomeDocument(best.getChromosome()));
        document.setFitness(optimizationMapper.toFitnessDocument(fitness));
        document.setCurrentPosition(optimizationMapper.toPositionDocument(fitness.getCurrentPosition()));
        document.setPositions(optimizationMapper.toPositionDocuments(fitness.getPositions()));

        optimizationRepository.save(document);
    }

    private OptimizationDocument createNewOptimization(OptimizationRequest request, AsyncProcess process) {
        OptimizationDocument document = new OptimizationDocument();
        document.setId(process.getProcessId());
        document.setStarted(System.currentTimeMillis());
        document.setParams(optimizationMapper.toParams(request));
        return optimizationRepository.save(document);
    }
}
