package eu.verante.trader.optimization.service;

import eu.verante.trader.decision.domain.DecisionFitness;
import eu.verante.trader.decision.domain.DecisionStats;
import eu.verante.trader.decision.domain.Position;
import eu.verante.trader.decision.domain.Transaction;
import eu.verante.trader.optimization.domain.*;
import eu.verante.trader.optimization.domain.api.ManualExecutionRequest;
import eu.verante.trader.optimization.domain.api.OptimizationRequest;
import eu.verante.trader.optimization.model.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING,
        uses = ToMapMapper.class)
public abstract class OptimizationMapper {

    @Autowired
    private ToMapMapper toMapMapper;

    public abstract Optimization toOptimization(OptimizationDocument document);

    public TradingGene toTradingGene(TradingGeneDocument document) {
        if (document instanceof IndicatorTradingGeneDocument) {
            return toIndicatorGene((IndicatorTradingGeneDocument) document);
        }
        throw new RuntimeException("unsupported: " + document);
    }

    public abstract IndicatorTradingGene toIndicatorGene(IndicatorTradingGeneDocument document);

    public List<Optimization> toOptimizations(List<OptimizationDocument> documents) {
        return documents.stream()
                .map(this::toOptimization)
                .collect(Collectors.toList());
    }

    public abstract OptimizationParams toParams(OptimizationRequest request);
    public abstract ManualExecutionParams toParams(ManualExecutionRequest request);

    public abstract TradingChromosomeDocument toChromosomeDocument(TradingChromosome chromosome);

    public abstract IndicatorTradingGeneDocument toIndicatorGeneDocument(IndicatorTradingGene gene);
    public TradingGeneDocument toGeneDocument(TradingGene gene) {
        if (gene instanceof IndicatorTradingGene) {
            return toIndicatorGeneDocument((IndicatorTradingGene) gene);
        }
        throw new RuntimeException("unsupported: " + gene.getClass().getName());
    }


    public abstract DecisionFitnessDocument toFitnessDocument(DecisionFitness fitness);

    public abstract DecisionStatsDocument toStatsDocument(DecisionStats stats);

    public abstract PositionDocument toPositionDocument(Position position);

    @Mapping(target = "time", expression="java(transaction.getTime().toInstant().toEpochMilli())")
    public abstract TransactionDocument toTransactionDocument(Transaction transaction);

    public List<PositionDocument> toPositionDocuments(List<Position> positions) {
        return positions.stream()
                .map(this::toPositionDocument)
                .collect(Collectors.toList());
    }

    public abstract Execution toExecution(ManualExecutionDocument document);

}
