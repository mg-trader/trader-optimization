package eu.verante.trader.optimization.service.client;

import eu.verante.trader.indicator.domain.IndicatorResource;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "indicator", url = "${clients.indicator.url}")
public interface IndicatorClient extends IndicatorResource {

}
