package eu.verante.trader.optimization.service.engine.indicator;

import eu.verante.trader.indicator.domain.Indicator;
import eu.verante.trader.optimization.domain.CompareGeneConfiguration;
import eu.verante.trader.optimization.domain.IndicatorGeneElement;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class CompareIndicatorGetter extends AbstractIndicatorGetter<CompareGeneConfiguration> {
    protected CompareIndicatorGetter() {
        super("COMPARE_INDICATORS");
    }

    @Override
    protected boolean filterByConfig(Indicator i, CompareGeneConfiguration configuration, IndicatorFactory factory) {
        Indicator value = factory.getIndicator(configuration.getValue());
        Indicator compareTo = factory.getIndicator(configuration.getCompareTo());


        return i.getConfiguration().get("value").equals(value.getName())
                && i.getConfiguration().get("compareTo").equals(compareTo.getName());
    }

    @Override
    protected Map<String, Object> createNewConfiguration(IndicatorGeneElement<CompareGeneConfiguration> geneElement, IndicatorFactory factory) {
        CompareGeneConfiguration configuration = geneElement.getConfiguration();
        Indicator value = factory.getIndicator(configuration.getValue());
        Indicator compareTo = factory.getIndicator(configuration.getCompareTo());

        return Map.of("value", value.getName(),
                "compareTo", compareTo.getName());
    }
}
