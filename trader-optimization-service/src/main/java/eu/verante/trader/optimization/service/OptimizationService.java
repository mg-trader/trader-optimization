package eu.verante.trader.optimization.service;

import eu.verante.trader.optimization.domain.Optimization;
import eu.verante.trader.optimization.domain.api.OptimizationRequest;
import eu.verante.trader.optimization.repository.OptimizationRepository;
import eu.verante.trader.util.asyncprocess.AsyncProcess;
import eu.verante.trader.util.asyncprocess.AsyncProcessHandler;
import eu.verante.trader.util.asyncprocess.AsyncProcessManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
public class OptimizationService {

    @Autowired
    private AsyncProcessManager processManager;

    @Autowired
    private AsyncProcessHandler<OptimizationRequest> optimizationHandler;

    @Autowired
    private OptimizationRepository repository;

    @Autowired
    private OptimizationMapper mapper;

    public AsyncProcess runOptimization(OptimizationRequest request) {
        return processManager.runProcess(request, optimizationHandler);
    }

    public AsyncProcess getProcessStatus(UUID processId) {
        return processManager.getById(processId);
    }

    public Optimization getById(UUID processId) {
        return mapper.toOptimization(repository.findById(processId).orElseThrow());
    }

    public List<Optimization> getAll() {
        return mapper.toOptimizations(repository.findAll());
    }
}
