package eu.verante.trader.optimization.service.engine.indicator;

import eu.verante.trader.indicator.domain.Indicator;
import eu.verante.trader.optimization.domain.IndicatorGeneConfiguration;
import eu.verante.trader.optimization.domain.IndicatorGeneElement;

import java.util.Collection;

public interface IndicatorGetter<C extends IndicatorGeneConfiguration<C>> {

    Collection<String> getHandledIndicatorTypes();

    Indicator getIndicator(IndicatorGeneElement<C> geneElement, IndicatorFactory factory);
}
