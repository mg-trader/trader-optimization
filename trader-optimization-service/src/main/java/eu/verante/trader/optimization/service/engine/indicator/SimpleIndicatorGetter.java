package eu.verante.trader.optimization.service.engine.indicator;

import eu.verante.trader.indicator.domain.Indicator;
import eu.verante.trader.optimization.domain.IndicatorGeneElement;
import eu.verante.trader.optimization.domain.SimpleGeneConfiguration;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class SimpleIndicatorGetter extends AbstractIndicatorGetter<SimpleGeneConfiguration> {

    protected SimpleIndicatorGetter() {
        super("SIMPLE");
    }

    @Override
    protected boolean filterByConfig(Indicator indicator, SimpleGeneConfiguration configuration, IndicatorFactory factory) {
        return indicator.getConfiguration().get("value").equals(configuration.getValue().name());
    }

    @Override
    protected Map<String, Object> createNewConfiguration(IndicatorGeneElement<SimpleGeneConfiguration> geneElement, IndicatorFactory factory) {
        return Map.of("value", geneElement.getConfiguration().getValue());
    }
}
