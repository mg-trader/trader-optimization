package eu.verante.trader.optimization;

import eu.verante.trader.optimization.domain.IndicatorGeneConfiguration;
import eu.verante.trader.optimization.domain.util.IndicatorGeneConfigurationDeserializer;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//@Configuration
public class ObjectMapperConfiguration {

//    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jsonCustomizer(IndicatorGeneConfigurationDeserializer deserializer) {
        return builder -> builder.deserializerByType(IndicatorGeneConfiguration.class, deserializer);
    }
}
