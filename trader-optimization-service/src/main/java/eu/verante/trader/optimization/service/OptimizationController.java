package eu.verante.trader.optimization.service;

import eu.verante.trader.optimization.domain.Optimization;
import eu.verante.trader.optimization.domain.api.OptimizationRequest;
import eu.verante.trader.optimization.domain.api.OptimizationResource;
import eu.verante.trader.util.asyncprocess.AsyncProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/optimization")
public class OptimizationController implements OptimizationResource {
    @Autowired
    private OptimizationService optimizationService;

    @Override
    public AsyncProcess runOptimization(OptimizationRequest request) {
        return optimizationService.runOptimization(request);
    }

    @Override
    public AsyncProcess getProcessStatus(UUID processId) {
        return optimizationService.getProcessStatus(processId);
    }

    @Override
    public Optimization getOptimizationById(UUID processId) {
        return optimizationService.getById(processId);
    }

    @Override
    public List<Optimization> getOptimizations() {
        return optimizationService.getAll();
    }
}
