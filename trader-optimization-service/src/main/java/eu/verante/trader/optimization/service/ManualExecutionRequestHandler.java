package eu.verante.trader.optimization.service;

import eu.verante.trader.decision.domain.DecisionFitness;
import eu.verante.trader.optimization.domain.TradingChromosome;
import eu.verante.trader.optimization.domain.api.ManualExecutionRequest;
import eu.verante.trader.optimization.model.ManualExecutionDocument;
import eu.verante.trader.optimization.model.OptimizationDocument;
import eu.verante.trader.optimization.repository.ManualExecutionRepository;
import eu.verante.trader.optimization.service.engine.fitness.DecisionFitnessCalculator;
import eu.verante.trader.optimization.service.engine.population.PopulationMember;
import eu.verante.trader.util.asyncprocess.AbstractAsyncProcessHandler;
import eu.verante.trader.util.asyncprocess.AsyncProcess;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ManualExecutionRequestHandler extends AbstractExecutionRequestHandler<ManualExecutionRequest> {

    @Autowired
    private ManualExecutionRepository repository;
    @Autowired
    private OptimizationService optimizationService;
    @Autowired
    private OptimizationMapper optimizationMapper;
    @Autowired
    private BeanFactory beanFactory;

    @Override
    protected void doProcess(ManualExecutionRequest request, AsyncProcess process) {
        ManualExecutionDocument newExecution = createNewDocument(request, process);

        cleanUpIfNecessary(request);

        TradingChromosome chromosome = optimizationService.getById(request.getBaseOptimizationId()).getChromosome();
        DecisionFitnessCalculator calculator = beanFactory.getBean(DecisionFitnessCalculator.class, request);
        DecisionFitness decisionFitness = calculator.get(chromosome);
        //execution


        System.out.println("Executed CHROMOSOME: " + chromosome);
        System.out.println("Executed PERFORMANCE: " + decisionFitness.getTotalStats().getProfitPercent());
        System.out.println("Executed TRADES");
        decisionFitness.getPositions().forEach(p -> System.out.println(p));
        System.out.println("Current trade:");
        System.out.println(decisionFitness.getCurrentPosition());
        System.out.println("Total stats: " +decisionFitness.getTotalStats());
        System.out.println("Buy stats: " + decisionFitness.getBuyStats());
        System.out.println("Sell stats: " + decisionFitness.getSellStats());

        storeResults(newExecution, chromosome, decisionFitness);
    }

    private void storeResults(ManualExecutionDocument execution, TradingChromosome chromosome, DecisionFitness decisionFitness) {
        execution.setEnded(System.currentTimeMillis());
        execution.setChromosome(optimizationMapper.toChromosomeDocument(chromosome));

        execution.setFitness(optimizationMapper.toFitnessDocument(decisionFitness));
        execution.setCurrentPosition(optimizationMapper.toPositionDocument(decisionFitness.getCurrentPosition()));
        execution.setPositions(optimizationMapper.toPositionDocuments(decisionFitness.getPositions()));

        repository.save(execution);
    }

    private ManualExecutionDocument createNewDocument(ManualExecutionRequest request, AsyncProcess process) {
        ManualExecutionDocument document = new ManualExecutionDocument();
        document.setId(process.getProcessId());
        document.setStarted(System.currentTimeMillis());
        document.setParams(optimizationMapper.toParams(request));
        return repository.save(document);
    }
}
