package eu.verante.trader.optimization.service.initial;

import eu.verante.trader.indicator.domain.IndicatorCategory;
import eu.verante.trader.indicator.domain.IndicatorType;
import eu.verante.trader.indicator.domain.value.SimpleValueType;
import eu.verante.trader.optimization.domain.*;
import eu.verante.trader.optimization.service.client.IndicatorTypeClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static eu.verante.trader.optimization.domain.IndicatorGeneElement.movingAverage;

@Component
public class TrendOnMovingAverageFactory implements InitialTradingGeneFactory {

    @Autowired
    private IndicatorTypeClient indicatorTypeClient;

    @Override
    public List<TradingGene> generateInitialGenes() {
        List<TradingGene> result = new ArrayList<>();

        indicatorTypeClient.getIndicators(IndicatorCategory.MOVING_AVERAGE).stream()
                .forEach(type -> {
                    result.addAll(generateWithThresholds(type));
                });
        return result;
    }

    private static Collection<? extends TradingGene> generateWithThresholds(IndicatorType type) {
        List<TradingGene> result = new ArrayList<>();
        result.addAll(trendOnMovingAverages(type, 5, 0.5));
        result.addAll(trendOnMovingAverages(type, 10, 3.0));
        return result;
    }

    private static List<IndicatorTradingGene> trendOnMovingAverages(IndicatorType type, int periods, double threshold) {
        List<IndicatorTradingGene> result = new ArrayList<>();
        for (int shift = -2 ; shift <=2 ; shift++) {
            result.add(new IndicatorTradingGene(10,
                    new TrendIndicatorGeneElement(
                            new TrendGeneConfiguration(movingAverage(type.getType(), periods, shift, SimpleValueType.CLOSE), threshold)
                )));
        }
        return result;
    }
}
