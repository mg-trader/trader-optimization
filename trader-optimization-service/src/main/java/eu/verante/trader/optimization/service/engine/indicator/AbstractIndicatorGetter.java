package eu.verante.trader.optimization.service.engine.indicator;

import eu.verante.trader.indicator.domain.Indicator;
import eu.verante.trader.indicator.domain.IndicatorCategory;
import eu.verante.trader.indicator.domain.request.CreateIndicatorRequest;
import eu.verante.trader.optimization.service.client.IndicatorClient;
import eu.verante.trader.optimization.service.client.IndicatorTypeClient;
import eu.verante.trader.optimization.domain.IndicatorGeneConfiguration;
import eu.verante.trader.optimization.domain.IndicatorGeneElement;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class AbstractIndicatorGetter<C extends IndicatorGeneConfiguration<C>>
        implements IndicatorGetter<C> {

    @Autowired
    private IndicatorClient indicatorClient;

    @Autowired
    private IndicatorTypeClient indicatorTypeClient;

    private Collection<String> handledTypes;

    private IndicatorCategory indicatorCategory;

    protected AbstractIndicatorGetter(String handledType) {
        this.handledTypes = Collections.singletonList(handledType);
    }

    protected AbstractIndicatorGetter(IndicatorCategory category) {
        this.indicatorCategory = category;
    }

    @Override
    public Indicator getIndicator(IndicatorGeneElement<C> geneElement, IndicatorFactory factory) {
        return getIndicatorClient().getAllIndicators(geneElement.getIndicatorType())
                .stream()
                .filter(i -> filterByConfig(i, geneElement.getConfiguration(), factory))
                .findAny()
                .orElseGet(() -> createNewIndicator(geneElement, factory));
    }

    protected abstract boolean filterByConfig(Indicator i, C configuration, IndicatorFactory factory);

    private Indicator createNewIndicator(IndicatorGeneElement<C> geneElement, IndicatorFactory factory) {
        CreateIndicatorRequest request = new CreateIndicatorRequest();
        long time = System.currentTimeMillis();
        request.setName(geneElement.getIndicatorType()+ "-generated-"+ time);
        request.setIndicatorType(geneElement.getIndicatorType());
        request.setDescription("Indicator created for simulation run at: " + time);
        request.setConfiguration(createNewConfiguration(geneElement, factory));

        getIndicatorClient().createNewIndicator(request);
        return getIndicatorClient().getIndicatorByName(request.getName());
    }

    protected abstract Map<String, Object> createNewConfiguration(IndicatorGeneElement<C> geneElement, IndicatorFactory factory);

    @Override
    public Collection<String> getHandledIndicatorTypes() {
        if (handledTypes == null) {
            handledTypes = indicatorTypeClient.getIndicators(indicatorCategory).stream()
                    .map(it -> it.getType())
                    .collect(Collectors.toList());
        }
        return handledTypes;
    }

    public IndicatorClient getIndicatorClient() {
        return indicatorClient;
    }
}
