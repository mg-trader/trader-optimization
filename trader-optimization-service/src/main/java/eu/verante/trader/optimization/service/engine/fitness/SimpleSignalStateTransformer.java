package eu.verante.trader.optimization.service.engine.fitness;

import eu.verante.trader.decision.domain.DecisionPredicate;
import eu.verante.trader.decision.domain.DecisionType;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;

public class SimpleSignalStateTransformer extends AbstractTradeStateTransformer {

    private DecisionType previousBuySellDecision = DecisionType.HOLD;
    @Override
    public DecisionPredicate transformNext(List<Pair<Integer, DecisionPredicate>> geneDecisions) {
        DecisionPredicate decision = getChromosomeDecision(geneDecisions);
        if (decision.getType() != DecisionType.HOLD) {
            previousBuySellDecision = decision.getType();
        }
        return new DecisionPredicate(previousBuySellDecision, decision.getContext());
    }
}
