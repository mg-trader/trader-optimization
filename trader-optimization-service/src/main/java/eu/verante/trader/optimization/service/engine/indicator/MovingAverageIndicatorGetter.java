package eu.verante.trader.optimization.service.engine.indicator;

import eu.verante.trader.indicator.domain.Indicator;
import eu.verante.trader.indicator.domain.IndicatorCategory;
import eu.verante.trader.optimization.domain.IndicatorGeneElement;
import eu.verante.trader.optimization.domain.MovingAverageConfiguration;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class MovingAverageIndicatorGetter
        extends AbstractIndicatorGetter<MovingAverageConfiguration> {

    protected MovingAverageIndicatorGetter() {
        super(IndicatorCategory.MOVING_AVERAGE);
    }

    @Override
    protected boolean filterByConfig(Indicator i, MovingAverageConfiguration configuration, IndicatorFactory factory) {
        return i.getConfiguration().get("periods").equals(configuration.getPeriods());
    }

    @Override
    protected Map<String, Object> createNewConfiguration(IndicatorGeneElement<MovingAverageConfiguration> geneElement, IndicatorFactory factory) {
        return Map.of("periods", geneElement.getConfiguration().getPeriods());
    }
}
