package eu.verante.trader.optimization.service.engine.fitness;

import eu.verante.trader.decision.domain.DecisionPredicate;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChromosomeDecision {

    private final List<Pair<Integer, DecisionPredicate>> geneDecisions = new ArrayList<>();

    void putDecision(int weight, DecisionPredicate decision) {
        geneDecisions.add(Pair.of(weight, decision));
    }

    public List<Pair<Integer, DecisionPredicate>> getGeneDecisions() {
        return Collections.unmodifiableList(geneDecisions);
    }
}
