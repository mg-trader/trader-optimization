package eu.verante.trader.optimization.service.engine.crosser;

import eu.verante.trader.optimization.service.engine.population.Population;
import eu.verante.trader.optimization.service.engine.population.PopulationMember;
import eu.verante.trader.optimization.domain.TradingChromosome;
import eu.verante.trader.optimization.domain.TradingGene;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class RandomPopulationCrosser implements PopulationCrosser {

    private final Random random = new Random(System.currentTimeMillis());

    @Override
    public Population cross(Population population) {
        List<PopulationMember> originalMembers = new ArrayList<>(population.findBest(population.size()));
        Collections.shuffle(originalMembers);

        List<PopulationMember> crossedMembers = new ArrayList<>();

        for (int i = 0; i < originalMembers.size(); i = i+2) {
            TradingChromosome member1 = originalMembers.get(i).getChromosome();
            TradingChromosome member2 = originalMembers.get(Math.min(i+1, originalMembers.size()-1)).getChromosome();

            List<TradingGene> genes1 = new ArrayList<>();
            List<TradingGene> genes2 = new ArrayList<>();
            for (int geneIndex = 0; geneIndex < member1.getTradingGenes().size() ; geneIndex++) {
                if (random.nextBoolean()) {
                    genes1.add(member1.getTradingGenes().get(geneIndex));
                    genes2.add(member2.getTradingGenes().get(geneIndex));
                } else {
                    genes1.add(member2.getTradingGenes().get(geneIndex));
                    genes2.add(member1.getTradingGenes().get(geneIndex));
                }
            }

            crossedMembers.add(new PopulationMember(new TradingChromosome(genes1)));
            crossedMembers.add(new PopulationMember(new TradingChromosome(genes2)));
        }
        originalMembers.addAll(crossedMembers);
        return Population.fromMembers(originalMembers);
    }
}
