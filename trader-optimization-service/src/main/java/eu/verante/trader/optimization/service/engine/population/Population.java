package eu.verante.trader.optimization.service.engine.population;

import eu.verante.trader.decision.domain.DecisionFitness;
import eu.verante.trader.optimization.service.engine.fitness.DecisionFitnessCalculator;
import eu.verante.trader.optimization.domain.TradingChromosome;

import java.util.*;
import java.util.stream.Collectors;

public class Population implements Iterable<PopulationMember>{

    private final List<PopulationMember> members;

    public static Population fromChromosomes(Collection<TradingChromosome> chromosomes) {
        return new Population(chromosomes.stream()
                .map(PopulationMember::new)
                .collect(Collectors.toList()));
    }

    public static Population fromMembers(Collection<PopulationMember> members) {
        return new Population(new ArrayList<>(members));
    }

    private Population(List<PopulationMember> members) {
        this.members = new ArrayList<>(members);
    }

    public void applyFitness(DecisionFitnessCalculator calculator) {
        members.parallelStream()
                .forEach(member -> {
                    doApplyFitness(calculator, member);
                });
    }

    private void doApplyFitness(DecisionFitnessCalculator calculator, PopulationMember member) {
        if (member.getFitness().isEmpty()) {
            DecisionFitness fitness = calculator.get(member.getChromosome());
            member.setFitness(fitness);
        }
    }
    public PopulationMember findBest(){
        return findBest(1).get(0);
    }

    public List<PopulationMember> findBest(int count) {
        members.sort(new PopulationMemberComparator());
        return members.subList(0, count);
    }

    public PopulationMember get(int index) {
        return members.get(index);
    }

    public int size() {
        return members.size();
    }

    @Override
    public Iterator<PopulationMember> iterator() {
        return members.iterator();
    }
}
