package eu.verante.trader.optimization.service.engine.crosser;

import eu.verante.trader.optimization.service.engine.population.Population;

public interface PopulationCrosser {

    Population cross(Population population);
}
