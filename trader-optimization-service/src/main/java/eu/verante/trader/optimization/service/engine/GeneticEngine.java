package eu.verante.trader.optimization.service.engine;

import eu.verante.trader.optimization.domain.api.OptimizationRequest;
import eu.verante.trader.optimization.service.engine.crosser.PopulationCrosser;
import eu.verante.trader.optimization.service.engine.crosser.RandomPopulationCrosser;
import eu.verante.trader.optimization.service.engine.fitness.DecisionFitnessCalculator;
import eu.verante.trader.optimization.service.engine.mutator.PopulationMutator;
import eu.verante.trader.optimization.service.engine.mutator.RandomPopulationMutator;
import eu.verante.trader.optimization.service.engine.population.Population;
import eu.verante.trader.optimization.service.engine.population.PopulationMember;
import eu.verante.trader.optimization.service.engine.selector.SimpleSurvivalPopulationSelector;
import eu.verante.trader.optimization.service.engine.selector.SurvivalPopulationSelector;
import eu.verante.trader.optimization.domain.TradingChromosome;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GeneticEngine {

    private final Log logger = LogFactory.getLog(GeneticEngine.class);

    @Autowired
    private BeanFactory beanFactory;


    public PopulationMember findBest(List<TradingChromosome> chromosomes,
                                     OptimizationRequest request) {
        Population population = Population.fromChromosomes(chromosomes);
        DecisionFitnessCalculator calculator = beanFactory.getBean(DecisionFitnessCalculator.class, request);
        SurvivalPopulationSelector selector = new SimpleSurvivalPopulationSelector(
                Math.min(50, request.getInitialPopulationSize()/4));
        PopulationMutator mutator = new RandomPopulationMutator(request.getMutationPromile());
        PopulationCrosser crosser = new RandomPopulationCrosser();

        for (int generation = 0; generation< request.getGenerations() ; generation ++) {
            logger.info("Processing generation: " + generation);
            population.applyFitness(calculator);
            PopulationMember currentBest = population.findBest();
            logger.info("Population's best fitness:" + currentBest.getFitness().get().getTotalStats().getProfitPercent());
            logger.info("Population's best chromosome: " + currentBest.getChromosome());
            logger.info("Population's best chromosome's current trade: " + currentBest.getFitness().get().getCurrentPosition());
            if (generation != request.getGenerations() -1 ) {
                population = selector.select(population);
                population = mutator.mutate(population);
                population = crosser.cross(population);
            }
        }
        return population.findBest();
    }
}
