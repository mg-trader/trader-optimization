package eu.verante.trader.optimization.service.engine.indicator;

import eu.verante.trader.indicator.domain.Indicator;
import eu.verante.trader.optimization.domain.IndicatorGeneElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Component
public class IndicatorFactory {

    private final Map<String, IndicatorGetter<?>> indicatorGetters;

    @Autowired
    public IndicatorFactory(Collection<IndicatorGetter<?>> indicatorGetters) {
        Map<String, IndicatorGetter<?>> getters = new HashMap<>();
        for (IndicatorGetter<?> indicatorGetter : indicatorGetters) {
            for (String type : indicatorGetter.getHandledIndicatorTypes()) {
                getters.put(type, indicatorGetter);
            }
        }

        this.indicatorGetters = getters;
    }

    @Cacheable("indicatorCache")
    public Indicator getIndicator(IndicatorGeneElement geneElement) {
        return indicatorGetters.get(geneElement.getIndicatorType())
                .getIndicator(geneElement, this);
    }
}
