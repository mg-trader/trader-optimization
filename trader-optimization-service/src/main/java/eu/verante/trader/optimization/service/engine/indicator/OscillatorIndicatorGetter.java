package eu.verante.trader.optimization.service.engine.indicator;

import eu.verante.trader.indicator.domain.Indicator;
import eu.verante.trader.indicator.domain.IndicatorCategory;
import eu.verante.trader.optimization.domain.IndicatorGeneElement;
import eu.verante.trader.optimization.domain.SimpleOscillatorConfiguration;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class OscillatorIndicatorGetter extends AbstractIndicatorGetter<SimpleOscillatorConfiguration> {
    protected OscillatorIndicatorGetter() {
        super(IndicatorCategory.OSCILLATOR);
    }

    @Override
    protected boolean filterByConfig(Indicator i, SimpleOscillatorConfiguration configuration, IndicatorFactory factory) {
        return i.getConfiguration().get("periods").equals(configuration.getPeriods());
    }

    @Override
    protected Map<String, Object> createNewConfiguration(IndicatorGeneElement<SimpleOscillatorConfiguration> geneElement, IndicatorFactory factory) {
        return Map.of("periods", geneElement.getConfiguration().getPeriods());
    }
}
