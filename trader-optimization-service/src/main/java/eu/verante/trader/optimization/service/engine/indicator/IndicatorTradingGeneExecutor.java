package eu.verante.trader.optimization.service.engine.indicator;

import eu.verante.trader.decision.domain.DecideByIndicatorRequest;
import eu.verante.trader.decision.domain.DecisionPredicate;
import eu.verante.trader.indicator.domain.Indicator;
import eu.verante.trader.optimization.domain.api.AbstractSingleExecutionRequest;
import eu.verante.trader.optimization.service.client.IndicatorDecisionClient;
import eu.verante.trader.optimization.service.engine.fitness.AbstractTradingGeneExecutor;
import eu.verante.trader.optimization.domain.IndicatorTradingGene;
import eu.verante.trader.util.asyncprocess.AsyncProcess;
import eu.verante.trader.util.asyncprocess.AsyncProcessAwaitFactory;
import eu.verante.trader.util.asyncprocess.AsyncProcessStatusChecker;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

@Component
public class IndicatorTradingGeneExecutor extends AbstractTradingGeneExecutor<IndicatorTradingGene> {

    private final Log logger = LogFactory.getLog(IndicatorTradingGeneExecutor.class);

    @Autowired
    private IndicatorFactory indicatorFactory;
    @Autowired
    private IndicatorDecisionClient indicatorDecisionClient;

    @Autowired
    private AsyncProcessAwaitFactory awaitFactory;

    public IndicatorTradingGeneExecutor() {
        super(IndicatorTradingGene.class);
    }

    @Override
    @Cacheable("decisionCache")
    public SortedMap<Long, DecisionPredicate> execute(IndicatorTradingGene gene, AbstractSingleExecutionRequest optimizationRequest) {
        Indicator indicator = indicatorFactory.getIndicator(gene.getIndicator());
        logger.debug("Executing gene for indicator: " + indicator.getName());

        DecideByIndicatorRequest request = new DecideByIndicatorRequest();
        request.setIndicator(indicator.getName());
        request.setFrom(optimizationRequest.getFrom());
        request.setTo(optimizationRequest.getTo());
        request.setPeriod(optimizationRequest.getPeriod());
        request.setSymbol(optimizationRequest.getSymbol());
        request.setConfiguration(gene.getIndicator().getConfiguration().asDecisionMakerConfiguration());

        AsyncProcess process = indicatorDecisionClient.decide(request);
        try {
            awaitFactory.createFor(createStatusChecker(process.getProcessId())).get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return new TreeMap<>(indicatorDecisionClient.findByRequest(request));
    }

    private AsyncProcessStatusChecker createStatusChecker(UUID processId) {
        return () -> indicatorDecisionClient.getProcessStatus(processId);
    }
}
