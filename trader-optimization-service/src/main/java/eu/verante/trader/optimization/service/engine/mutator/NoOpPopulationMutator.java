package eu.verante.trader.optimization.service.engine.mutator;

import eu.verante.trader.optimization.service.engine.population.Population;

public class NoOpPopulationMutator implements PopulationMutator{
    @Override
    public Population mutate(Population population) {
        return population;
    }
}
