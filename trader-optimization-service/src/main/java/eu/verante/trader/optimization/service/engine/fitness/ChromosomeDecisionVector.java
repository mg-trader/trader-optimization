package eu.verante.trader.optimization.service.engine.fitness;

import eu.verante.trader.decision.domain.DecisionPredicate;

import java.util.SortedMap;
import java.util.TreeMap;

public class ChromosomeDecisionVector {
    private final SortedMap<Long, ChromosomeDecision> decisions = new TreeMap<>();

    ChromosomeDecision getChromosomeDecision(Long time) {
        return decisions.computeIfAbsent(time, t -> new ChromosomeDecision());
    }

    public SortedMap<Long, DecisionPredicate> toTradeStates(TradeStateTransformer stateTransformer) {
        SortedMap<Long, DecisionPredicate> states = new TreeMap<>();

        for (Long time : decisions.keySet()) {
            DecisionPredicate decision = stateTransformer.transformNext(decisions.get(time).getGeneDecisions());
            states.put(time, new DecisionPredicate(decision.getType(), decision.getContext()));
        }
        return states;
    }

}
