package eu.verante.trader.optimization.service.client;

import eu.verante.trader.decision.domain.IndicatorDecisionResource;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "indicatorDecision", url = "${clients.indicatorDecision.url}")
public interface IndicatorDecisionClient extends IndicatorDecisionResource {

}
