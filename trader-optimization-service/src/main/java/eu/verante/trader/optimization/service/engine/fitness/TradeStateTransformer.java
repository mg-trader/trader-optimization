package eu.verante.trader.optimization.service.engine.fitness;

import eu.verante.trader.decision.domain.DecisionPredicate;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;

public interface TradeStateTransformer {

    DecisionPredicate transformNext(List<Pair<Integer, DecisionPredicate>> geneDecisions);
}
