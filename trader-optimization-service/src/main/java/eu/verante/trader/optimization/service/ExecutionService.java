package eu.verante.trader.optimization.service;

import eu.verante.trader.optimization.domain.Execution;
import eu.verante.trader.optimization.domain.Optimization;
import eu.verante.trader.optimization.domain.api.BatchExecutionRequest;
import eu.verante.trader.optimization.domain.api.ManualExecutionRequest;
import eu.verante.trader.optimization.domain.api.OptimizationRequest;
import eu.verante.trader.optimization.model.ManualExecutionDocument;
import eu.verante.trader.optimization.repository.ManualExecutionRepository;
import eu.verante.trader.optimization.service.OptimizationMapper;
import eu.verante.trader.util.asyncprocess.AsyncProcess;
import eu.verante.trader.util.asyncprocess.AsyncProcessHandler;
import eu.verante.trader.util.asyncprocess.AsyncProcessManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class ExecutionService {

    @Autowired
    private ManualExecutionRepository repository;

    @Autowired
    private OptimizationMapper mapper;

    @Autowired
    private AsyncProcessManager processManager;

    @Autowired
    private AsyncProcessHandler<ManualExecutionRequest> executionHandler;
    @Autowired
    private AsyncProcessHandler<BatchExecutionRequest> batchExecutionHandler;

    public Execution getById(UUID id) {
        ManualExecutionDocument document = repository.findById(id).orElseThrow();
        return mapper.toExecution(document);
    }
    
    public List<Execution> getAll() {
        return repository.findAll().stream()
                .map(mapper::toExecution)
                .collect(Collectors.toList());
    }

    public AsyncProcess runExecution(ManualExecutionRequest request) {
        return processManager.runProcess(request, executionHandler);
    }

    public AsyncProcess runExecutions(BatchExecutionRequest request) {
        return processManager.runProcess(request, batchExecutionHandler);
    }
}
