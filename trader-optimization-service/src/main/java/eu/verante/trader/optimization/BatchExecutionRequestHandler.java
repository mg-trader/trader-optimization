package eu.verante.trader.optimization;

import eu.verante.trader.decision.domain.DecisionFitness;
import eu.verante.trader.optimization.domain.Optimization;
import eu.verante.trader.optimization.domain.TradingChromosome;
import eu.verante.trader.optimization.domain.api.BatchExecutionRequest;
import eu.verante.trader.optimization.domain.api.ManualExecutionRequest;
import eu.verante.trader.optimization.model.ManualExecutionDocument;
import eu.verante.trader.optimization.repository.ManualExecutionRepository;
import eu.verante.trader.optimization.service.AbstractExecutionRequestHandler;
import eu.verante.trader.optimization.service.OptimizationMapper;
import eu.verante.trader.optimization.service.OptimizationService;
import eu.verante.trader.optimization.service.engine.fitness.DecisionFitnessCalculator;
import eu.verante.trader.util.asyncprocess.AsyncProcess;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class BatchExecutionRequestHandler extends AbstractExecutionRequestHandler<BatchExecutionRequest> {

    @Autowired
    private OptimizationService optimizationService;
    @Autowired
    private ManualExecutionRepository repository;
    @Autowired
    private BeanFactory beanFactory;
    @Autowired
    private OptimizationMapper optimizationMapper;
    @Override
    protected void doProcess(BatchExecutionRequest request, AsyncProcess process) {
        cleanUpIfNecessary(request);

        List<Optimization> optimizations = optimizationService.getAll().stream()
                .filter(o -> o.getFitness() != null)
                .filter(o -> request.getSymbols().contains(o.getParams().getSymbol()))
                .toList();
        List<ManualExecutionDocument> fitnesses = optimizations.stream()
                .map(o -> runExecution(o, request))
                .collect(Collectors.toList());
        fitnesses.forEach( fitness -> {
            StringBuilder builder = new StringBuilder();
            builder.append(fitness.getParams().getSymbol())
                    .append(";")
                    .append(fitness.getFitness().getTotalStats().getProfitPercent())
                    .append(";")
                    .append(fitness.getCurrentPosition().getPositionType())
                    .append(";")
                    .append(ZonedDateTime.ofInstant(
                            Instant.ofEpochMilli(
                                    fitness.getCurrentPosition().getOpen().getTime()), ZoneId.systemDefault()))
                    .append(";")
                    .append(fitness.getCurrentPosition().getOpen().getPrice());
            System.out.println(builder);
        });
    }

    private ManualExecutionDocument runExecution(Optimization optimization, BatchExecutionRequest request) {
        logger.info("Running execution for: " + optimization.getParams().getSymbol()+ " % " + optimization.getId());
        ManualExecutionRequest manualRequest = getManualRequest(request, optimization);
        ManualExecutionDocument newExecution = createNewDocument(manualRequest, optimization);

        TradingChromosome chromosome = optimization.getChromosome();
        DecisionFitnessCalculator calculator = beanFactory.getBean(DecisionFitnessCalculator.class, manualRequest);
        DecisionFitness decisionFitness = calculator.get(chromosome);
        storeResults(newExecution, chromosome, decisionFitness);
        return newExecution;
    }

    private ManualExecutionDocument createNewDocument(ManualExecutionRequest request, Optimization optimization) {
        ManualExecutionDocument document = new ManualExecutionDocument();
        document.setId(UUID.randomUUID());
        document.setStarted(System.currentTimeMillis());

        document.setParams(optimizationMapper.toParams(request));
        return repository.save(document);
    }

    private static ManualExecutionRequest getManualRequest(BatchExecutionRequest request, Optimization optimization) {
        ManualExecutionRequest manualRequest = new ManualExecutionRequest();
        manualRequest.setSymbol(optimization.getParams().getSymbol());
        manualRequest.setFrom(request.getFrom());
        manualRequest.setTo(request.getTo());
        manualRequest.setPeriod(request.getPeriod());
        manualRequest.setBaseOptimizationId(optimization.getId());
        manualRequest.setCleanupBefore(request.isCleanupBefore());
        return manualRequest;
    }

    private void storeResults(ManualExecutionDocument execution, TradingChromosome chromosome, DecisionFitness decisionFitness) {
        execution.setEnded(System.currentTimeMillis());
        execution.setChromosome(optimizationMapper.toChromosomeDocument(chromosome));

        execution.setFitness(optimizationMapper.toFitnessDocument(decisionFitness));
        execution.setCurrentPosition(optimizationMapper.toPositionDocument(decisionFitness.getCurrentPosition()));
        execution.setPositions(optimizationMapper.toPositionDocuments(decisionFitness.getPositions()));

        repository.save(execution);
    }
}
