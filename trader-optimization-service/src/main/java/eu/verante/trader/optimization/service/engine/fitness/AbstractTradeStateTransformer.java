package eu.verante.trader.optimization.service.engine.fitness;

import eu.verante.trader.decision.domain.DecisionPredicate;
import eu.verante.trader.decision.domain.DecisionType;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractTradeStateTransformer implements TradeStateTransformer {

    protected DecisionPredicate getChromosomeDecision(List<Pair<Integer, DecisionPredicate>> geneDecisions) {
        List<String> context = geneDecisions.stream()
                .flatMap(d -> d.getRight().getContext().stream())
                .collect(Collectors.toList());
        int sum = geneDecisions.stream()
                .mapToInt(pair -> pair.getKey() * getDecisionAsNumber(pair.getValue().getType()))
                .sum();
        return getDecisionFromNumber(sum, context);
    }

    private DecisionPredicate getDecisionFromNumber(int sum, List<String> context) {
        if (sum > 0) {
            return new DecisionPredicate(DecisionType.BUY, context);
        }
        if (sum < 0) {
            return new DecisionPredicate(DecisionType.SELL, context);
        }
        return new DecisionPredicate(DecisionType.HOLD, context);
    }

    private Integer getDecisionAsNumber(DecisionType value) {
        return switch (value) {
            case BUY -> 1;
            case SELL -> -1;
            default -> 0;
        };
    }
}
