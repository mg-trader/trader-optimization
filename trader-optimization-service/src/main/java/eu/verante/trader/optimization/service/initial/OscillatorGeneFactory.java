package eu.verante.trader.optimization.service.initial;

import eu.verante.trader.optimization.domain.IndicatorTradingGene;
import eu.verante.trader.optimization.domain.SimpleOscillatorConfiguration;
import eu.verante.trader.optimization.domain.OscillatorGeneElement;
import eu.verante.trader.optimization.domain.TradingGene;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//@Component
public class OscillatorGeneFactory implements InitialTradingGeneFactory {

    static Map<Integer, Double> momentumConfigurations = Map.of(
            5, 7.5,
            10, 10.0,
            15, 12.0,
            20, 15.0,
            30, 20.0,
            50, 25.0
    );

    @Override
    public List<TradingGene> generateInitialGenes() {
        List<TradingGene> result = new ArrayList<>();
        for (Map.Entry<Integer, Double> config : momentumConfigurations.entrySet()) {
            result.addAll(generateMomentumGenes(config.getKey(), config.getValue()));
            result.addAll(generateCCIGenes(config.getKey()));
        }
        return result;
    }

    private List<TradingGene> generateCCIGenes(int periods) {
        List<TradingGene> result = new ArrayList<>();
        result.add(new IndicatorTradingGene(10,
                new OscillatorGeneElement("CCI",
                        new SimpleOscillatorConfiguration(periods, 100.0*0.8)
                )));
        result.add(new IndicatorTradingGene(10,
                new OscillatorGeneElement("CCI",
                        new SimpleOscillatorConfiguration(periods, 100.0)
                )));
        result.add(new IndicatorTradingGene(10,
                new OscillatorGeneElement("CCI",
                        new SimpleOscillatorConfiguration(periods, 100.0*1.2)
                )));
        return result;
    }

    private static List<TradingGene> generateMomentumGenes(int periods, double threshold) {
        List<TradingGene> result = new ArrayList<>();
        result.add(new IndicatorTradingGene(10,
                new OscillatorGeneElement("MOMENTUM",
                        new SimpleOscillatorConfiguration(periods, threshold*0.8)
                )));
        result.add(new IndicatorTradingGene(10,
                new OscillatorGeneElement("MOMENTUM",
                        new SimpleOscillatorConfiguration(periods, threshold)
                )));
        result.add(new IndicatorTradingGene(10,
                new OscillatorGeneElement("MOMENTUM",
                        new SimpleOscillatorConfiguration(periods, threshold)
                )));
        return result;
    }
}
