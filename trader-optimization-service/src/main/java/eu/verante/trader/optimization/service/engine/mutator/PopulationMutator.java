package eu.verante.trader.optimization.service.engine.mutator;

import eu.verante.trader.optimization.service.engine.population.Population;

public interface PopulationMutator {

    Population mutate(Population population);
}
