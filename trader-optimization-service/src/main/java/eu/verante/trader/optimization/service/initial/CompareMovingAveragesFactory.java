package eu.verante.trader.optimization.service.initial;

import eu.verante.trader.indicator.domain.IndicatorCategory;
import eu.verante.trader.indicator.domain.IndicatorType;
import eu.verante.trader.indicator.domain.value.SimpleValueType;
import eu.verante.trader.optimization.service.client.IndicatorTypeClient;
import eu.verante.trader.optimization.domain.IndicatorTradingGene;
import eu.verante.trader.optimization.domain.TradingGene;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static eu.verante.trader.optimization.domain.IndicatorGeneElement.compare;
import static eu.verante.trader.optimization.domain.IndicatorGeneElement.movingAverage;

@Component
public class CompareMovingAveragesFactory implements InitialTradingGeneFactory {

    @Autowired
    private IndicatorTypeClient indicatorTypeClient;
    @Override
    public List<TradingGene> generateInitialGenes() {
        List<TradingGene> result = new ArrayList<>();
        Collection<IndicatorType> indicators = indicatorTypeClient.getIndicators(IndicatorCategory.MOVING_AVERAGE);
        indicators.forEach(type1 -> {
            indicators.forEach(type2 -> {
                    result.addAll(generateCompareMAGenes(type1.getType(), type2.getType(), 5, 10));
                    result.addAll(generateCompareMAGenes(type1.getType(), type2.getType(), 10, 20));
                    result.addAll(generateCompareMAGenes(type1.getType(), type2.getType(), 20, 50));
                });
        });
        return result;
    }

    private List<TradingGene> generateCompareMAGenes(String type, String compareType, int periods, int compareToPeriods) {
        List<TradingGene> result = new ArrayList<>();
        for (int shift1 = -2 ; shift1 <=2 ; shift1++) {
            for (int shift2 = -2 ; shift2 <=2 ; shift2++) {
                result.add(new IndicatorTradingGene(10,
                        compare(
                                movingAverage(type, periods, shift1, SimpleValueType.CLOSE),
                                movingAverage(compareType, compareToPeriods, shift2, SimpleValueType.CLOSE)
                        )));
            }
        }
        return result;
    }
}
