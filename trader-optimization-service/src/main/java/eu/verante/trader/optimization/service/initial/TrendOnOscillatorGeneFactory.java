package eu.verante.trader.optimization.service.initial;

import eu.verante.trader.optimization.domain.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static eu.verante.trader.optimization.service.initial.OscillatorGeneFactory.momentumConfigurations;

//@Component
public class TrendOnOscillatorGeneFactory implements InitialTradingGeneFactory {
    @Override
    public List<TradingGene> generateInitialGenes() {
        List<TradingGene> result = new ArrayList<>();
        for (Map.Entry<Integer, Double> config : momentumConfigurations.entrySet()) {
            for (double threshold = 0.1; threshold < 10.0 ; threshold=threshold*2.0) {
                result.add(new IndicatorTradingGene(10,
                        new TrendIndicatorGeneElement(new TrendGeneConfiguration(
                                new OscillatorGeneElement("MOMENTUM",
                                        new SimpleOscillatorConfiguration(config.getKey(), config.getValue())), threshold)
                        )));

                result.add(new IndicatorTradingGene(10,
                        new TrendIndicatorGeneElement(new TrendGeneConfiguration(
                                new OscillatorGeneElement("CCI",
                                        new SimpleOscillatorConfiguration(config.getKey(), 100.0)), threshold)
                        )));
            }
        }
        return result;
    }
}
