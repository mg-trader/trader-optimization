package eu.verante.trader.optimization.service.engine.fitness;

import eu.verante.trader.decision.domain.DecisionPredicate;
import eu.verante.trader.optimization.domain.api.AbstractSingleExecutionRequest;
import eu.verante.trader.optimization.domain.TradingGene;

import java.util.SortedMap;

public interface TradingGeneExecutor<TG extends TradingGene> {

    Class<TG> getHandledClass();

    SortedMap<Long, DecisionPredicate> execute(TG gene, AbstractSingleExecutionRequest request);
}
