package eu.verante.trader.optimization.service.client;

import eu.verante.trader.decision.domain.FitnessResource;
import org.springframework.cloud.openfeign.FeignClient;


@FeignClient(name = "fitness", url = "${clients.fitness.url}")
public interface FitnessClient extends FitnessResource {

}
