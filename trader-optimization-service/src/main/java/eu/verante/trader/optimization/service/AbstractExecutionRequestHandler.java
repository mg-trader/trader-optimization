package eu.verante.trader.optimization.service;

import eu.verante.trader.optimization.domain.api.AbstractExecutionRequest;
import eu.verante.trader.optimization.service.client.IndicatorCalculationClient;
import eu.verante.trader.optimization.service.client.IndicatorDecisionClient;
import eu.verante.trader.util.asyncprocess.AbstractAsyncProcessHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;

public abstract class AbstractExecutionRequestHandler<R extends AbstractExecutionRequest>
        extends AbstractAsyncProcessHandler<R> {

    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private IndicatorDecisionClient decisionClient;

    @Autowired
    private IndicatorCalculationClient calculationClient;

    @Autowired
    private CacheManager cacheManager;

    protected void cleanUpIfNecessary(R request) {
        if (request.isCleanupBefore()) {
            logger.info("Cleaning up services");
            calculationClient.cleanupAllCalculations();
            decisionClient.cleanupAllDecisions();
            cacheManager.getCacheNames()
                    .forEach(name -> cacheManager.getCache(name).clear());
        }
    }
}
