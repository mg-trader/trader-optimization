package eu.verante.trader.optimization.service;

import eu.verante.trader.optimization.domain.Execution;
import eu.verante.trader.optimization.domain.api.BatchExecutionRequest;
import eu.verante.trader.optimization.domain.api.ExecutionResource;
import eu.verante.trader.optimization.domain.api.ManualExecutionRequest;
import eu.verante.trader.util.asyncprocess.AsyncProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/execution")
public class ExecutionController implements ExecutionResource {

    @Autowired
    private ExecutionService executionService;
    @Override
    public Execution getExecution(UUID id) {
        return executionService.getById(id);
    }

    @Override
    public List<Execution> getExecutions() {
        return executionService.getAll();
    }

    @Override
    public AsyncProcess runExecution(ManualExecutionRequest request) {
        return executionService.runExecution(request);
    }

    @Override
    public AsyncProcess runAllExecutions(BatchExecutionRequest request) {
        return executionService.runExecutions(request);
    }
}
