package eu.verante.trader.optimization.service.engine.mutator;

import eu.verante.trader.optimization.service.engine.GeneticEngine;
import eu.verante.trader.optimization.service.engine.population.Population;
import eu.verante.trader.optimization.service.engine.population.PopulationMember;
import eu.verante.trader.optimization.domain.TradingChromosome;
import eu.verante.trader.optimization.domain.TradingGene;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class SimpleWeightMutator implements PopulationMutator{

    private final Log logger = LogFactory.getLog(GeneticEngine.class);

    private static final SecureRandom random = new SecureRandom(BigInteger.valueOf(System.currentTimeMillis()).toByteArray());

    private final int mutationPromile;

    public SimpleWeightMutator(int mutationPromile) {
        this.mutationPromile = mutationPromile;
    }

    @Override
    public Population mutate(Population population) {
        List<PopulationMember> newMembers = new ArrayList<>();
        for (PopulationMember populationMember : population) {
            int randomValue = random.nextInt(1000);
            if (randomValue < mutationPromile) {
                newMembers.add(mutateMember(populationMember));
            } else {
                newMembers.add(populationMember);
            }
        }

        return Population.fromMembers(newMembers);
    }

    private PopulationMember mutateMember(PopulationMember populationMember) {
        List<TradingGene> originalGenes = populationMember.getChromosome().getTradingGenes();
        List<TradingGene> newGenes = new ArrayList<>();
        int geneToMutate = random.nextInt(originalGenes.size());
        for (int geneIndex = 0; geneIndex < originalGenes.size(); geneIndex++) {
            TradingGene original = originalGenes.get(geneIndex);
            if (geneIndex == geneToMutate) {
                newGenes.add(mutateGene(original));
            } else {
                newGenes.add(original);
            }
        }
        return new PopulationMember(new TradingChromosome(newGenes));
    }

    private TradingGene mutateGene(TradingGene original) {

        int weightChange = random.nextBoolean() ? 1 : -1;
        int newWeight = original.getWeight() + weightChange;

        TradingGene gene = original.withWeight(newWeight);
        logger.info("mutated gene to new weight: " + newWeight + " - " + gene);

        return gene;
    }
}
