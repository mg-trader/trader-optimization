package eu.verante.trader.optimization.service.client;

import eu.verante.trader.indicator.domain.CalculationResource;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "indicatorCalculation", url = "${clients.indicatorCalculation.url}")
public interface IndicatorCalculationClient extends CalculationResource {
}
