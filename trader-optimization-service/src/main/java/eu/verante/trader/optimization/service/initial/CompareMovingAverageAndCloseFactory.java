package eu.verante.trader.optimization.service.initial;

import eu.verante.trader.indicator.domain.IndicatorCategory;
import eu.verante.trader.indicator.domain.value.SimpleValueType;
import eu.verante.trader.optimization.service.client.IndicatorTypeClient;
import eu.verante.trader.optimization.domain.IndicatorTradingGene;
import eu.verante.trader.optimization.domain.TradingGene;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static eu.verante.trader.optimization.domain.IndicatorGeneElement.*;

@Component
public class CompareMovingAverageAndCloseFactory implements InitialTradingGeneFactory {
    @Autowired
    private IndicatorTypeClient indicatorTypeClient;
    @Override
    public List<TradingGene> generateInitialGenes() {
        List<TradingGene> result = new ArrayList<>();
        indicatorTypeClient.getIndicators(IndicatorCategory.MOVING_AVERAGE)
                .forEach(type -> {
                    result.addAll(generateCompareMAAndCloseGenes(type.getType(), 5));
                    result.addAll(generateCompareMAAndCloseGenes(type.getType(), 10));
                    result.addAll(generateCompareMAAndCloseGenes(type.getType(), 20));
                });
        return result;
    }

    private List<TradingGene> generateCompareMAAndCloseGenes(String maType, int periods) {
        List<TradingGene> result = new ArrayList<>();
        for (int shift = -2 ; shift <=2 ; shift++) {
            result.add(new IndicatorTradingGene(10,
                    compare(
                            simpleClose(),
                            movingAverage(maType, periods, shift, SimpleValueType.CLOSE))
            ));
        }
        return result;
    }
}
