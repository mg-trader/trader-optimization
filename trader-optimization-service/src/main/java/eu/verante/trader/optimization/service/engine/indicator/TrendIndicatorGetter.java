package eu.verante.trader.optimization.service.engine.indicator;

import eu.verante.trader.indicator.domain.Indicator;
import eu.verante.trader.optimization.domain.IndicatorGeneElement;
import eu.verante.trader.optimization.domain.TrendGeneConfiguration;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class TrendIndicatorGetter extends AbstractIndicatorGetter<TrendGeneConfiguration> {
    protected TrendIndicatorGetter() {
        super("TREND");
    }

    @Override
    protected boolean filterByConfig(Indicator i, TrendGeneConfiguration configuration, IndicatorFactory factory) {
        Indicator innerIndicator = factory.getIndicator(configuration.getIndicator());
        return i.getConfiguration().get("indicator").equals(innerIndicator.getName())
                && i.getConfiguration().get("threshold").equals(configuration.getThreshold());

    }

    @Override
    protected Map<String, Object> createNewConfiguration(IndicatorGeneElement<TrendGeneConfiguration> geneElement, IndicatorFactory factory) {
        TrendGeneConfiguration configuration = geneElement.getConfiguration();

        Indicator innerIndicator = factory.getIndicator(configuration.getIndicator());

        return Map.of("indicator",innerIndicator.getName(),
                "threshold", configuration.getThreshold());
    }
}
