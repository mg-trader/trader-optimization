package eu.verante.trader.optimization.service.initial;

import eu.verante.trader.optimization.domain.TradingGene;

import java.util.List;

public interface InitialTradingGeneFactory {

    List<TradingGene> generateInitialGenes();
}
