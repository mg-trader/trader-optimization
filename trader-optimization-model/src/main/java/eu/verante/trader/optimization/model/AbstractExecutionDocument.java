package eu.verante.trader.optimization.model;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.List;
import java.util.UUID;

@Data
public abstract class AbstractExecutionDocument<P extends AbstractExecutionParams> {

    @Id
    private UUID id;

    private long started;
    private long ended;
    private P params;

    private TradingChromosomeDocument chromosome;

    private DecisionFitnessDocument fitness;

    private List<PositionDocument> positions;
    private PositionDocument currentPosition;
}
