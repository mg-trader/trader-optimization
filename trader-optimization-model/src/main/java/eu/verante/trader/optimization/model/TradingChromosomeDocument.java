package eu.verante.trader.optimization.model;

import lombok.Data;

import java.util.List;

@Data
public class TradingChromosomeDocument {

    private List<TradingGeneDocument> tradingGenes;
}
