package eu.verante.trader.optimization.model;

import lombok.Data;

@Data
public abstract class TradingGeneDocument {

    int weight;
}
