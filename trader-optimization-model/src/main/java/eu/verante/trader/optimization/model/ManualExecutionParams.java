package eu.verante.trader.optimization.model;

import lombok.Data;

import java.util.UUID;

@Data
public class ManualExecutionParams extends AbstractExecutionParams {
    private UUID baseOptimizationId;
}
