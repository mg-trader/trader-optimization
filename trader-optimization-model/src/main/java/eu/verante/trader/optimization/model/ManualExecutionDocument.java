package eu.verante.trader.optimization.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Document("manual-execution")
@Data
public class ManualExecutionDocument extends AbstractExecutionDocument<ManualExecutionParams>{
}
