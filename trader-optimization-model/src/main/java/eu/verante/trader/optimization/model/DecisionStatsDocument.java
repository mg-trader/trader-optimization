package eu.verante.trader.optimization.model;

import lombok.Data;

@Data
public class DecisionStatsDocument {
    private long positions;
    private long profitablePositions;
    private double profitablePositionsPercent;
    private double profit;
    private double profitPercent;
}
