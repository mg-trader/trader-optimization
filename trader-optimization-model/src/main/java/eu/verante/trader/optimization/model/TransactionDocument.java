package eu.verante.trader.optimization.model;

import lombok.Data;

import java.util.List;

@Data
public class TransactionDocument {
    private String type;
    private long time;
    private double price;
    private double volume;
    private List<String> context;
}
