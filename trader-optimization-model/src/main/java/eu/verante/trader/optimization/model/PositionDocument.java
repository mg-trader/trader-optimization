package eu.verante.trader.optimization.model;

import lombok.Data;

@Data
public class PositionDocument {
    private String positionType;
    private double profit;
    private double profitPercent;
    private TransactionDocument open;
    private TransactionDocument close;
}
