package eu.verante.trader.optimization.repository;

import eu.verante.trader.optimization.model.OptimizationDocument;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.UUID;

public interface OptimizationRepository extends MongoRepository<OptimizationDocument, UUID> {
}
