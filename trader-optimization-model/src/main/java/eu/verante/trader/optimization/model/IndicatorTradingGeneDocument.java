package eu.verante.trader.optimization.model;

import lombok.Data;

import java.util.Map;

@Data
public class IndicatorTradingGeneDocument extends TradingGeneDocument {

    private Map<String, Object> indicator;
}
