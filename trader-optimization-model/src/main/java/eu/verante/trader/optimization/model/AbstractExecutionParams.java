package eu.verante.trader.optimization.model;

import lombok.Data;

@Data
public abstract class AbstractExecutionParams {

    private long from;
    private long to;
    private String symbol;
    private String period;

    private boolean cleanupBefore;
}
