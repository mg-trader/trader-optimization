package eu.verante.trader.optimization.repository;

import eu.verante.trader.optimization.model.ManualExecutionDocument;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.UUID;

public interface ManualExecutionRepository extends MongoRepository<ManualExecutionDocument, UUID> {
}
