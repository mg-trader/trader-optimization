package eu.verante.trader.optimization.model;

public enum ExecutionTypeDefinition {
    OPTIMIZATION,
    MANUAL
}
