package eu.verante.trader.optimization.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Document("optimization")
@Data
public class OptimizationDocument extends AbstractExecutionDocument<OptimizationParams>{

}
