package eu.verante.trader.optimization.model;

import lombok.Data;

@Data
public class OptimizationParams extends AbstractExecutionParams {
    private int initialPopulationSize = 10000;
    private int generations = 10;

    private int mutationPromile = 10;
}
