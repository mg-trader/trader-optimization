package eu.verante.trader.optimization.model;

import lombok.Data;

@Data
public class DecisionFitnessDocument {
    private DecisionStatsDocument totalStats;
    private DecisionStatsDocument buyStats;
    private DecisionStatsDocument sellStats;
}
