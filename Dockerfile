FROM openjdk:19-alpine

EXPOSE 10001
ARG JAR_FILE=trader-optimization-service/target/trader-optimization-service-1.0-SNAPSHOT.jar
ADD ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]